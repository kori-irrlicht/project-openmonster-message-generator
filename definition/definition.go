package definition

type Definition struct {
	Title       string                `json:"title"`
	Description string                `json:"description"`
	Type        string                `json:"type"`
	KeyType     string                `json:"keytype,omitempty"`
	Properties  map[string]Definition `json:"properties,omitempty"`
	Required    []string              `json:"required,omitempty"`
	Items       *Definition           `json:"items,omitempty"`
	DataType    bool                  `json:"dataType,omitempty"`
	Validation  Validation            `json:"validation,omitempty"`
}

type Formatter interface {
	Format(Definition) ([]byte, error)
}

type Writer interface {
	Write(data []byte, filename string) error
}

type Validation struct {
	PositiveInt bool `json:"positiveInt,omitempty"`
	Nested      bool `json:"nested,omitempty"`
	RangeType   bool `json:"rangeType,omitempty"`

	NestedArray bool `json:"-"`
}
