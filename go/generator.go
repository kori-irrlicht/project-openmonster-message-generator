package golang

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os/exec"
	"path/filepath"
	"strings"
	"text/template"

	"gitlab.com/kori-irrlicht/project-openmonster-message-generator/definition"
)

type Struct struct {
	Name       string
	Comment    string
	Attributes []Attribute

	CreateMessageType bool
}

type Attribute struct {
	Name    string
	Type    string
	Tags    []Tag
	Comment string

	Validation definition.Validation
}

type Tag struct {
	Name       string
	Properties []string
}

var StructTemplate = `
{{if .CreateMessageType}}const Type_{{.Name}} string = "{{.Name}}"{{end}}
{{if .Comment}}// {{.Comment}} {{end}}
type {{.Name}} struct {
{{range .Attributes}}
    {{if .Comment}}// {{.Comment}} {{end}}
    {{.Name}} {{.Type}} {{if .Tags}}` + "`" + `{{range .Tags}}{{.Name}}:"{{range .Properties}}{{.}},{{end}}" {{end}}` + "`" + `{{end}}
{{end}}
}

func (data {{.Name}}) Validate() error {

    //Declare test functions
    checkPositiveInt := func(i int)bool{return i >= 0}
    _ = checkPositiveInt

    //Validate attributes
    {{range .Attributes}}
      {{if .Validation.RangeType}}
	for _, v := range data.{{.Name}}{
          {{if .Validation.NestedArray}}
	     for _, v := range v{
	  {{end}}

	{{if .Validation.PositiveInt}}
	    if !checkPositiveInt(v){return errors.New("Attribute '{{.Name}}' failed the validation check: positiveInt")}
	{{end}}
	{{if .Validation.Nested}}
	    if err := v.Validate(); err != nil {return fmt.Errorf("Attribute '{{.Name}}' failed a nested validation check: %w", err)}
	{{end}}

          {{if .Validation.NestedArray}}
	     } 
	  {{end}}

	}
      {{else}}
	{{if .Validation.PositiveInt}}
	    if !checkPositiveInt(data.{{.Name}}){return errors.New("Attribute '{{.Name}}' failed the validation check: positiveInt")}
	{{end}}
	{{if .Validation.Nested}}
	    if err := data.{{.Name}}.Validate(); err != nil {return fmt.Errorf("Attribute '{{.Name}}' failed a nested validation check: %w", err)}
	{{end}}
      {{end}}
    {{end}}
    return nil
}
`

type Writer struct {
	Package   string
	Directory string
}

func (w Writer) Write(data []byte, filename string) error {
	fp := filepath.Clean(filepath.Join(w.Directory, filename))

	d := fmt.Sprintf(`package %s
import (
 "errors"
 "fmt"
)

%s`, w.Package, string(data))

	if err := ioutil.WriteFile(fp, []byte(d), 0644); err != nil {
		return err
	}

	cmd := exec.Command("goimports", "-w", fp)
	return cmd.Run()

}

type Formatter struct {
}

func (f Formatter) Format(def definition.Definition) ([]byte, error) {
	s := Struct{
		Name:              def.Title,
		Comment:           def.Description,
		CreateMessageType: !def.DataType,
	}
	for k, v := range def.Properties {
		name := strings.ToUpper(k[:1]) + k[1:]
		s.Attributes = append(s.Attributes, Attribute{
			Name:    name,
			Type:    jsonTypeToGoType(v),
			Comment: v.Description,
			Tags: []Tag{
				{
					Name: "json",
					Properties: []string{
						k,
						"omitempty",
					},
				},
			},
			Validation: v.Validation,
		})
		attr := &s.Attributes[len(s.Attributes)-1]
		if strings.Count(attr.Type, "[") == 2 {
			attr.Validation.NestedArray = true
		}
	}

	for _, v := range def.Required {
		for kAttr, attr := range s.Attributes {
			name := strings.ToLower(attr.Name[:1]) + attr.Name[1:]
			if name == v {
				for kTag, tag := range attr.Tags {
					for i, prop := range tag.Properties {
						if prop == "omitempty" {
							tag.Properties = append(tag.Properties[:i], tag.Properties[i+1:]...)
							attr.Tags[kTag] = tag
							s.Attributes[kAttr] = attr
						}
					}
				}
			}
		}
	}

	var buf bytes.Buffer
	t := template.Must(template.New("struct").Parse(StructTemplate))
	err := t.Execute(&buf, s)
	return buf.Bytes(), err
}

func basicTypeConversion(t string) string {
	switch t {
	case "string":
		return "string"
	case "integer":
		return "int"
	case "number":
		return "float64"
	case "boolean":
		return "bool"

	default:
		return ""
	}

}

func jsonTypeToGoType(def definition.Definition) string {
	t := basicTypeConversion(def.Type)
	if len(t) > 0 {
		return t
	}

	split := strings.Split(def.Type, ":")
	if len(split) > 1 && split[0] == "custom" {
		return split[1]
	}

	switch def.Type {
	case "array":
		return "[]" + jsonTypeToGoType(*def.Items)

	case "map":
		return "map[" + basicTypeConversion(def.Items.KeyType) + "]" + jsonTypeToGoType(*def.Items)
	default:
		return "interface{}"
	}

}
