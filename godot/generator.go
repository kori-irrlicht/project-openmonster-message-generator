package godot

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"text/template"

	"gitlab.com/kori-irrlicht/project-openmonster-message-generator/definition"
)

type Writer struct {
	Directory string
	Filename  string
}

func (w Writer) Write(data []byte, filename string) error {
	fp := filepath.Clean(filepath.Join(w.Directory, w.Filename))
	file, err := os.OpenFile(fp, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0600)
	if err != nil {
		return err
	}

	_, err = file.Write(data)

	return err

}

type Data struct {
	Name       string
	Comment    string
	Attributes []Attribute

	CreateMessageType bool
}

type Attribute struct {
	Name       string
	Type       string
	Comment    string
	NestedType string

	Validation definition.Validation
}

type Formatter struct {
}

func (f Formatter) Format(def definition.Definition) ([]byte, error) {
	s := Data{
		Name:              def.Title,
		Comment:           def.Description,
		CreateMessageType: !def.DataType,
	}

	var keys []string
	for k, _ := range def.Properties {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	for _, k := range keys {
		v := def.Properties[k]
		name := strings.ToUpper(k[:1]) + k[1:]
		s.Attributes = append(s.Attributes, Attribute{
			Name:       name,
			Type:       jsonTypeToGodotType(v),
			Comment:    v.Description,
			Validation: v.Validation,
		})
		attr := &s.Attributes[len(s.Attributes)-1]

		if attr.Validation.RangeType {
			attr.NestedType = jsonTypeToGodotType(*v.Items)
		}

		if attr.NestedType == "Array" {
			attr.Validation.NestedArray = true
			attr.NestedType = jsonTypeToGodotType(*v.Items.Items)
		}

	}

	file, err := ioutil.ReadFile("./godot/godot.template")
	if err != nil {
		return nil, fmt.Errorf("could not read template file: %w", err)
	}

	var buf bytes.Buffer
	t := template.Must(template.New("struct").Parse(string(file)))
	err = t.Execute(&buf, s)
	return buf.Bytes(), err
}

func basicTypeConversion(t string) string {
	switch t {
	case "string":
		return "String"
	case "integer":
		return "int"
	case "number":
		return "float"
	case "boolean":
		return "bool"

	default:
		return ""
	}

}

func jsonTypeToGodotType(def definition.Definition) string {
	t := basicTypeConversion(def.Type)
	if len(t) > 0 {
		return t
	}

	split := strings.Split(def.Type, ":")
	if len(split) > 1 && split[0] == "custom" {
		return split[1]
	}

	switch def.Type {
	case "array":
		return "Array"

	case "map":
		return "Dictionary"
	default:
		return "Variant"
	}

}
