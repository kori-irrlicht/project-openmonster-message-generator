package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/kori-irrlicht/project-openmonster-message-generator/definition"
	golang "gitlab.com/kori-irrlicht/project-openmonster-message-generator/go"
	"gitlab.com/kori-irrlicht/project-openmonster-message-generator/godot"
	"log"
	"os/exec"
)

var (
	lang      string
	pkg       string
	outputDir string
	inputDir  string
)

func init() {
	const short = " (shorthand)"
	const usageLang = "Output language"
	flag.StringVar(&lang, "language", "", usageLang)
	flag.StringVar(&lang, "l", "", usageLang+short)

	const usagePkg = `If the target language supports packaging, this is used as the package name`
	flag.StringVar(&pkg, "package", "messages", usagePkg)
	flag.StringVar(&pkg, "p", "messages", usagePkg+short)

	const usageInputDir = "Read the schema files from this directory"
	flag.StringVar(&inputDir, "input", "./schema", usageInputDir)
	flag.StringVar(&inputDir, "i", "./schema", usageInputDir+short)

	const usageOutputDir = "Write the generated files to this directory"
	flag.StringVar(&outputDir, "output", "./messages", usageOutputDir)
	flag.StringVar(&outputDir, "o", "./messages", usageOutputDir+short)
}

func main() {
	flag.Parse()

	var form definition.Formatter
	var writer definition.Writer
	var fileEnding string

	stat, err := os.Stat(outputDir)
	if err != nil {

		if os.IsNotExist(err) {
			// Create dir
			if err := os.Mkdir(outputDir, 0766); err != nil {
				fmt.Println("Could not create outputDir: ", err)
				os.Exit(3)
			}

		} else {
			fmt.Println("Could not check on outputDir: ", err)
			os.Exit(4)
		}
	} else {

		if !stat.IsDir() {
			fmt.Printf("OutputDir '%s' is not a directory\n", outputDir)
			os.Exit(5)
		} else {
			if err := os.RemoveAll(outputDir); err != nil {
				fmt.Println("Could not clean outputDir: ", err)
				os.Exit(6)
			}
			// Create dir
			if err := os.Mkdir(outputDir, 0766); err != nil {
				fmt.Println("Could not create outputDir: ", err)
				os.Exit(3)
			}
		}
	}

	switch lang {
	case "go":
		form = golang.Formatter{}
		writer = golang.Writer{
			Package:   pkg,
			Directory: outputDir,
		}
		fileEnding = "go"

	case "godot":
		form = godot.Formatter{}
		writer = godot.Writer{
			Directory: outputDir,
			Filename:  "network.gd",
		}
		fileEnding = "gd"

	default:
		fmt.Println("Unsupported language")
		os.Exit(1)
		return
	}

	if err := filepath.Walk(inputDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() || !strings.HasSuffix(info.Name(), ".json") {
			return nil
		}

		file, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}

		var root definition.Definition
		if err := json.Unmarshal(file, &root); err != nil {
			return err
		}

		str, err := form.Format(root)
		if err != nil {
			return err
		}

		if err := writer.Write(str, strings.ToLower(root.Title)+"_generated."+fileEnding); err != nil {
			return err
		}
		return nil
	}); err != nil {
		fmt.Println(err)
		os.Exit(2)
	}

	switch lang {

	case "godot":
		w := writer.(godot.Writer)
		fp := filepath.Clean(filepath.Join(w.Directory, w.Filename))
		if _, err = exec.LookPath("gdformat"); err != nil {

			log.Println("Could not format output file: gdformat is not installed")
			return
		}

		cmd := exec.Command("gdformat", fp)
		if err := cmd.Run(); err != nil {
			log.Println("Could not format output file: " + err.Error())
		}

	default:
	}

}
